//
//  Extensions.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 09/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

extension BinaryInteger {
    var degreesToRadians: CGFloat { return CGFloat(Int(self)) * .pi / 180 }
}

extension SKSpriteNode {
    
    func aspectFillToSize(fillSize: CGSize) {
        if texture != nil {
            self.size = texture!.size()
            let verticalRatio = fillSize.height / self.texture!.size().height
            let horizontalRatio = fillSize.width /  self.texture!.size().width
            let scaleRatio = horizontalRatio > verticalRatio ? horizontalRatio : verticalRatio
            self.setScale(scaleRatio)
        }
    }
    
    func addGlow(size:CGSize, color:UIColor, radius: Float = 10) {
        let effectNode = SKEffectNode()
        effectNode.shouldRasterize = true
        let spriteNode = SKSpriteNode(color: color, size: size)
        effectNode.addChild(spriteNode)
        effectNode.filter = CIFilter(name: "CIGaussianBlur", withInputParameters: ["inputRadius":radius])
        addChild(effectNode)
    }
    
    func animateWobble() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            var actions:[SKAction] = []
            actions.append(SKAction.rotate(toAngle: 20.degreesToRadians, duration: 0.1, shortestUnitArc:true))
            actions.append(SKAction.rotate(toAngle: 340.degreesToRadians, duration: 0.2, shortestUnitArc:true))
            actions.append(SKAction.rotate(toAngle: 0.degreesToRadians, duration: 0.1, shortestUnitArc:true))
            actions.append(SKAction.rotate(toAngle: 20.degreesToRadians, duration: 0.1, shortestUnitArc:true))
            actions.append(SKAction.rotate(toAngle: 340.degreesToRadians, duration: 0.2, shortestUnitArc:true))
            actions.append(SKAction.rotate(toAngle: 0.degreesToRadians, duration: 0.1, shortestUnitArc:true))
            self.run(SKAction.sequence(actions), completion: {
                self.animateWobble()
            })
        }
    }
}

extension SKShapeNode {
    
    func addGlow(size:CGSize, color:UIColor, radius: Float = 10) {
        let effectNode = SKEffectNode()
        effectNode.shouldRasterize = true
        let spriteNode = SKSpriteNode(color: color, size: size)
        effectNode.addChild(spriteNode)
        effectNode.filter = CIFilter(name: "CIGaussianBlur", withInputParameters: ["inputRadius":radius])
        addChild(effectNode)
    }
}

extension UIColor {
    
    convenience init(rgbColorCodeRed red: Int, green: Int, blue: Int, alpha: CGFloat) {
        let redPart: CGFloat = CGFloat(red) / 255
        let greenPart: CGFloat = CGFloat(green) / 255
        let bluePart: CGFloat = CGFloat(blue) / 255
        self.init(red: redPart, green: greenPart, blue: bluePart, alpha: alpha)
    }
}

extension Double {
    
    func timeFormat() -> String {
        let minutes = Int(floor(self/Double(60)))
        let seconds = Int(self - Double(minutes * 60))
        return "\(minutes):\(seconds < 10 ? "0" : "")\(seconds)"
    }
}

extension SKNode {
        
    func squishAnimate(callback:@escaping (()->Void)) {
        run(SKAction.sequence([SKAction.scaleY(to: 0.8, duration: 0.1), SKAction.scaleY(to: 1.2, duration: 0.1), SKAction.scaleY(to: 1.0, duration: 0.1)])) {
            callback()
        }
    }
}

extension String {
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
}

extension UIFont {
    func fontThatFits(text:String, width : CGFloat) -> UIFont {
        var font = self
        while text.widthOfString(usingFont: font) > width {
            let newFontSize = font.pointSize - 1
            if newFontSize == 0 {
                return self
            }
            font = font.withSize(newFontSize)
        }
        return font
    }
}
