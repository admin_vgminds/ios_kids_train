//
//  Metrics.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 11/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit

struct Metrics {
    static var navBarHeight:CGFloat = 44
    static let half:CGFloat = 0.5
    static let baseHeight:CGFloat = 375
    static var factor:CGFloat = 1
}

struct Thumbnail {
    static var VerticalMargin:CGFloat = 55
    static var ContentEdgePadding:CGFloat = 20
    static var HorizontalPadding:CGFloat = 23
    static var ShadowSize:CGFloat = 9
    static var ImagePadding:CGFloat = 8.5
    static var InfoSectionHeight:CGFloat = 55.5
    static var ButtonWidth:CGFloat = 46
    static var InfoSectionBottomPadding:CGFloat = 18.5
    static var PlayIconWidth:CGFloat = 24
    static var FreeLabelWidth:CGFloat = 35
}

struct Popup {
    static var closeButtonWidth:CGFloat = 21
    static var closeButtonPadding:CGFloat = 10
}

struct Settings {
    static var pointerWidth:CGFloat = 10.5
    static var pointerHeight:CGFloat = 8
    static var eraseButtonWidth:CGFloat = 111.5
    static var eraseButtonHeight:CGFloat = 27
    static var eraseButtonBottomSpacing:CGFloat = 21
}
