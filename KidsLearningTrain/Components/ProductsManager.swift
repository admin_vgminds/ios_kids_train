//
//  ProductManager.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 18/03/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import StoreKit

class ProductsManager: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    var productsRequestStateLock = NSLock()
    var products:[SKProduct]?
    var request:SKProductsRequest?
    var delegate:ProductsManagerDelegate?
    
    let productIdentifiers = NSSet(objects: DataLoader.sharedInstance.adsInfo["NoAdsInAppId"] as! String, DataLoader.sharedInstance.adsInfo["DiscountedNoAdsInAppId"] as! String)
    
    func downloadProducts() {
        if !productsRequestStateLock.try() {
            print("Products request already in progress")
            return
        }
        print("Requesting products")
        request = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
        request?.delegate = self
        request?.start()
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        products = response.products
        DispatchQueue.main.async {
            self.productsRequestStateLock.unlock()
            guard let delegate = self.delegate else {
                return
            }
            if self.products != nil && self.products!.count > 0 {
                delegate.didDownloadProducts(self.products!)
            }  else {
                delegate.didDownloadProductsFailed()
            }
        }
    }
    
    func buy(_ product:SKProduct) {
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(payment)
    }
    
    fileprivate func donePurchase(_ queue: SKPaymentQueue, _ trans: SKPaymentTransaction) {
        queue.finishTransaction(trans)
        delegate?.didBuyProduct()
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                print("Purchased")
                donePurchase(queue, transaction)
            case .purchasing:
                print("Purchasing")
            case .failed:
                print("Failed")
                queue.finishTransaction(transaction)
                delegate?.didBuyProductFailed()
            case .restored:
                print("Restored")
                donePurchase(queue, transaction)
            case .deferred:
                print("Deferred")
            }
        }
    }
}

protocol ProductsManagerDelegate {
    func didDownloadProducts(_ products:[SKProduct])
    func didDownloadProductsFailed()
    func didBuyProduct()
    func didBuyProductFailed()
}
