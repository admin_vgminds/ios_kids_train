//
//  AdLoader.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 27/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import SuperAwesome
import Firebase
import GoogleMobileAds

class AdLoader : NSObject, GADInterstitialDelegate {
    private var viewController:UIViewController!
    private var delegate:AdLoaderDelegate!
    private var SAInterstitialAdLoading = false
    private var SAVideoAdLoading = false
    private var adexInterstitial: GADInterstitial!
    private var adexInterstitialDelegate: GADInterstitialDelegate!
    
    convenience init(viewController:UIViewController, delegate:AdLoaderDelegate) {
        self.init()
        self.delegate = delegate
        self.viewController = viewController
        initSA()
        initAdex()
    }
    
    func load() {
        if SAVideoAd.hasAdAvailable(Const.SUPER_AWESOME_VIDEO_ID) {
            SAVideoAd.play(Const.SUPER_AWESOME_VIDEO_ID, fromVC: self.viewController)
        } else if SAInterstitialAd.hasAdAvailable(Const.SUPER_AWESOME_INTERSTITIAL_ID) {
            SAInterstitialAd.play(Const.SUPER_AWESOME_INTERSTITIAL_ID, fromVC: self.viewController)
            loadSAVideoAd()
        } else if adexInterstitial != nil && adexInterstitial.isReady {
            adexInterstitial.present(fromRootViewController: self.viewController)
            loadSAVideoAd()
            loadSAInterstitialAd()
        } else {
            loadSAInterstitialAd()
            loadSAVideoAd()
            adexInterstitial = createAndLoadInterstitial()
            delegate.adLoaderFinished(success:false)
        }
    }
    
    // SA methods
    fileprivate func initSA() {
        // SuperAwesome interstitial ads
        SAInterstitialAd.setConfigurationProduction()
        //#if DEBUG
        //SAInterstitialAd.enableTestMode()
        //#endif
        if Const.debug {
            SAInterstitialAd.enableTestMode()
        } else {
            SAInterstitialAd.disableTestMode()
        }
        
        SAInterstitialAd.setCallback { (placementId, event) in
            switch event {
            case .adFailedToLoad:
                self.SAInterstitialAdLoading = false
            case .adEmpty:
                self.SAInterstitialAdLoading = false
            case .adAlreadyLoaded:
                self.SAInterstitialAdLoading = false
            case .adLoaded:
                self.SAInterstitialAdLoading = false
            case .adClosed:
                self.delegate.adLoaderFinished(success:true)
                self.loadSAInterstitialAd()
            case .adFailedToShow:
                self.delegate.adLoaderFinished(success: false)
            default:
                print("SuperAwesome Event - \(event)")
                self.delegate.adLoaderFinished(success: false)
            }
        }
        loadSAInterstitialAd()
        
        // SuperAwesome video ads
        SAVideoAd.setConfigurationProduction()
        //#if DEBUG
        //SAVideoAd.enableTestMode()
        //#endif
        if Const.debug {
            SAVideoAd.enableTestMode()
        } else {
            SAVideoAd.disableTestMode()
        }
        
        SAVideoAd.enableCloseButton()
        SAVideoAd.setCallback { (placementID, event) in
            switch event {
            case .adFailedToLoad:
                self.SAVideoAdLoading = false
            case .adEmpty:
                self.SAVideoAdLoading = false
            case .adAlreadyLoaded:
                self.SAVideoAdLoading = false
            case .adLoaded:
                self.SAVideoAdLoading = false
            case .adClosed:
                self.delegate.adLoaderFinished(success: true)
                self.loadSAVideoAd()
            case .adFailedToShow:
                self.delegate.adLoaderFinished(success: false)
            default:
                print("SuperAwesome Event: \(event)")
            }
        }
        loadSAVideoAd()
    }
    
    fileprivate func loadSAInterstitialAd() {
        if SAInterstitialAdLoading {
            return
        }
        SAInterstitialAdLoading = true
        SAInterstitialAd.load(Const.SUPER_AWESOME_INTERSTITIAL_ID)
    }
    
    fileprivate func loadSAVideoAd() {
        if SAVideoAdLoading {
            return
        }
        SAVideoAdLoading = true
        SAVideoAd.load(Const.SUPER_AWESOME_VIDEO_ID)
    }
    
    // Adex methods
    fileprivate func initAdex() {
        //GADMobileAds.configure(withApplicationID: Const.adexDebugAppId)
        adexInterstitial = createAndLoadInterstitial()
    }
    
    fileprivate func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial:GADInterstitial
        if Const.debug {
            interstitial = GADInterstitial(adUnitID: Const.adexDebugInterstitialId)
        } else {
            interstitial = GADInterstitial(adUnitID: Const.adexInterstitialId)
        }
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    /// GADInterstitialDelegate functions
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
        adexInterstitial = createAndLoadInterstitial()
        delegate.adLoaderFinished(success:true)
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
}

protocol AdLoaderDelegate {
    func adLoaderFinished(success:Bool)
}
