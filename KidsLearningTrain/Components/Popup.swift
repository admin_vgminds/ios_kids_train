//
//  Popup.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 20/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class PopUp {
    
    var overlay:SKSpriteNode!
    var contentArea:SKSpriteNode!
    var coloredBackground:SKShapeNode!
    var bgColor:UIColor!
    var scene:SKScene!
    var title:SKLabelNode!
    var closeButton:SKSpriteNode!
    var dismissable = true
    var size:CGSize
    
    init(view:SKView, bgColor:UIColor, size:CGSize, title:String = "", headerImageName:String? = nil, titleColor:UIColor = UIColor(rgbColorCodeRed: 61, green: 59, blue: 149, alpha: 1.0)) {
        self.bgColor = bgColor
        self.size = size
        
        addOverlay(view)
        addContentArea()
        addContentBackground(bgColor)
        let contentOffset = addHeaderImage(headerImageName)
        addCloseButton()
        addTitle(title, titleColor, contentOffset)
    }
    
    func present(scene:SKScene) {
        self.scene = scene
        scene.addChild(overlay)
        contentArea.run(SKAction.sequence([SKAction.scaleY(to: 0.9, duration: 0.1), SKAction.scaleY(to: 1.1, duration: 0.1), SKAction.scaleY(to: 1.0, duration: 0.1)]))
    }
    
    func remove(animate:Bool = true, onComplete:(()->Void)? = nil) {
        if !dismissable {
            return
        }
        AudioPlayer.playSound("Close")
        if !animate {
            self.overlay.removeFromParent()
            return
        }
        contentArea.run(SKAction.sequence([SKAction.scale(to: 1.1, duration: 0.1), SKAction.scale(to: 0.4, duration: 0.2)])) {
            self.overlay.removeFromParent()
            if onComplete != nil {
                onComplete!()
            }
        }
    }
    
    func changeBackgroundSize(size:CGSize) {
        let zPosition = coloredBackground.zPosition
        coloredBackground.removeFromParent()
        
        coloredBackground = SKShapeNode(rectOf: size, cornerRadius: 7.5 * Metrics.factor)
        coloredBackground.strokeColor = bgColor
        coloredBackground.fillColor = bgColor
        coloredBackground.position = CGPoint(x: 0, y: 0)
        coloredBackground.zPosition = zPosition
        contentArea.addChild(coloredBackground)
        
        closeButton.position = CGPoint(x: size.width * Metrics.half - Popup.closeButtonWidth * Metrics.half - Popup.closeButtonPadding, y: closeButton.position.y)
    }
    
    fileprivate func addOverlay(_ view: SKView) {
        let popupWidth:CGFloat = view.bounds.width
        let popupHeight:CGFloat = view.bounds.height
        overlay = SKSpriteNode(color: UIColor(rgbColorCodeRed: 0, green: 0, blue: 0, alpha: 0.54), size: CGSize(width:popupWidth, height: popupHeight))
        overlay.position = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
        overlay.name = "popup-background"
    }
    
    fileprivate func addContentArea() {
        contentArea = LayeredNode(color: UIColor.clear, size: size)
        contentArea.position = CGPoint(x: 0, y: 0)
        overlay.addChild(contentArea)
    }
    
    fileprivate func addContentBackground(_ bgColor: UIColor) {
        coloredBackground = SKShapeNode(rectOf: size, cornerRadius: 7.5 * Metrics.factor)
        coloredBackground.strokeColor = bgColor
        coloredBackground.fillColor = bgColor
        coloredBackground.position = CGPoint(x: 0, y: 0)
        contentArea.addChild(coloredBackground)
    }
    
    fileprivate func addHeaderImage(_ headerImageName: String?) -> CGFloat {
        if let headerImageName = headerImageName {
            let halfPopupContentHeight:CGFloat = size.height * Metrics.half
            let headerImageWidth:CGFloat = 83.5 * Metrics.factor
            let headerImage = SKSpriteNode(texture: SKTexture(imageNamed: headerImageName), color: UIColor.clear, size: CGSize(width:headerImageWidth, height:headerImageWidth))
            headerImage.position = CGPoint(x:0, y:halfPopupContentHeight)
            contentArea.addChild(headerImage)
            return -headerImageWidth * 0.5
        }
        return 0
    }
    
    fileprivate func addCloseButton() {
        let halfPopupContentHeight:CGFloat = size.height * Metrics.half
        let closeButton = SKSpriteNode(texture: SKTexture(imageNamed: "Close-Btn"), size: CGSize(width:Popup.closeButtonWidth, height:Popup.closeButtonWidth))
        closeButton.position = CGPoint(x: size.width * Metrics.half - Popup.closeButtonWidth * Metrics.half - Popup.closeButtonPadding, y: halfPopupContentHeight - Popup.closeButtonWidth * Metrics.half - Popup.closeButtonPadding)
        closeButton.name = "popup-close"
        contentArea.addChild(closeButton)
        self.closeButton = closeButton
    }
    
    fileprivate func addTitle(_ title: String, _ titleColor: UIColor, _ contentOffset: CGFloat) {
        let halfPopupContentHeight:CGFloat = size.height * Metrics.half
        let title = SKLabelNode(text: title)
        title.verticalAlignmentMode = .center
        title.horizontalAlignmentMode = .center
        title.fontName = "ProximaNova-Semibold"
        title.fontColor = titleColor
        let fontSize = 15.5 * Metrics.factor
        title.fontSize = fontSize
        title.position = CGPoint(x: 0, y: halfPopupContentHeight - 16 * Metrics.factor - fontSize * Metrics.half + contentOffset)
        contentArea.addChild(title)
        self.title = title
    }
}
