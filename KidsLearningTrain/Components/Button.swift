//
//  Button.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 11/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import SpriteKit

class Button: SKSpriteNode {
    
    private var clickAction:((_ name:String?)->Void)?
    private var buttonName:String?
    private var id:String?
    private var label:SKLabelNode?
    private var innerIcon:SKSpriteNode?
    private var radius:CGFloat?
    private var buttonBg:SKShapeNode?
    
    var animateClick:Bool = false
    var onBeforeClickAnimation:(()->Void)?
    
    convenience init(imageNamed:String, size:CGSize, name:String, fontName:String, fontColor:UIColor, fontSize:CGFloat, id:String? = nil) {
        self.init(texture: SKTexture(imageNamed:imageNamed), size: size)
        
        addLabel(name, fontName, fontSize, fontColor)
        setId(id)
        addDefaultClickHandler()
    }
    
    convenience init(foregroundImage:String, backgroundImage:String, size:CGSize, animate:Bool = false, id:String? = nil) {
        self.init(color: UIColor.clear, size: size)
        
        self.texture = SKTexture(image: UIImage(named:backgroundImage)!)
        
        let innerIconPadding:CGFloat = 7
        let innerIconHeight:CGFloat = size.height - innerIconPadding * CGFloat(2)
        let innerIcon = SKSpriteNode(color: UIColor.clear, size: CGSize(width: innerIconHeight, height: innerIconHeight))
        innerIcon.texture = SKTexture(image: UIImage(named:foregroundImage)!)
        innerIcon.position = CGPoint(x: 0, y: 0)
        innerIcon.name = id
        addChild(innerIcon)
        self.innerIcon = innerIcon

        setId(id)
        
        if animate {
            animateWobble()
        }
    }
    
    convenience init(backgroundColor:UIColor, color:UIColor, size:CGSize, name:String, fontName:String, fontColor:UIColor, fontSize:CGFloat, radius:CGFloat = 0, id:String? = nil) {
        self.init(color: backgroundColor, size: size);
        
        self.radius = radius
        setBackgroundColor(color)
        
        addLabel(name, fontName, fontSize, fontColor)
        setId(id)
        addDefaultClickHandler()
    }
    
    func onClick(_ clicked:@escaping ((_ name:String?)->Void)) {
        self.clickAction = clicked
    }
    
    func clicked() {
        guard let clickAction = self.clickAction else {
            return
        }
        AudioPlayer.playSound("ButtonClick")
        if animateClick {
            if let onBeforeClickAnimation = self.onBeforeClickAnimation {
                onBeforeClickAnimation()
            }
            squishAnimate {
                clickAction(self.buttonName)
            }
            return
        }
        clickAction(buttonName)
    }
    
    fileprivate func addLabel(_ name: String, _ fontName: String, _ fontSize: CGFloat, _ fontColor: UIColor) {
        if (name != "") {
            self.buttonName = name
            let label = SKLabelNode(text: name)
            label.position = CGPoint(x:0,y:0)
            label.horizontalAlignmentMode = .center
            label.verticalAlignmentMode = .center
            label.fontName = fontName
            label.fontSize = fontSize
            label.fontColor = fontColor
            addChild(label)
            self.label = label
        }
    }
    
    fileprivate func addDefaultClickHandler() {
        clickAction = {name in
            print("Set something for button click!")
        }
    }
    
    fileprivate func setId(_ id: String?) {
        if (id != nil && id != "") {
            if let label = self.label {
                label.name = id
            }
            if let innerIcon = self.innerIcon {
                innerIcon.name = id
            }
            self.name = id
        }
    }
    
    func setBackgroundColor(_ color:UIColor) {
        let buttonBg = SKShapeNode(rectOf: self.size, cornerRadius: self.radius!)
        buttonBg.strokeColor = color
        buttonBg.fillColor = color
        buttonBg.position = CGPoint.zero
        if let currentButtonBg = self.buttonBg {
            currentButtonBg.addChild(buttonBg)
        } else {
            self.addChild(buttonBg)
            self.buttonBg = buttonBg
        }
    }
    
    func setTitle(_ title:String) {
        if let label = self.label {
            label.text = title
        }
    }
    
}
