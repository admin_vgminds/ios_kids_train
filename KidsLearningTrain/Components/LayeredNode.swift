//
//  LayeredNode.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 01/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class LayeredNode: SKSpriteNode {
    
    var lastZPosition:CGFloat = 0
    
    override func addChild(_ node: SKNode) {
        lastZPosition = node.zPosition == 0 ? lastZPosition + 1 : node.zPosition
        node.zPosition = lastZPosition
        super.addChild(node)
    }
    
}
