//
//  Tab.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 01/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class Tab: SKSpriteNode {
    
    let highlightColor = UIColor.white
    let unHighlightColor = UIColor(rgbColorCodeRed: 209, green: 209, blue: 209, alpha: 1)
    let highlightTextColor = UIColor(rgbColorCodeRed: 61, green: 59, blue: 149, alpha: 1)
    let unHighlightTextColor = UIColor(rgbColorCodeRed: 103, green: 103, blue: 103, alpha: 1)
    
    var delegateClicked:((_ tabIndex:Int)->Void)!
    var tab1:SKSpriteNode!
    var tab2:SKSpriteNode!
    var tab1Label:SKLabelNode!
    var tab2Label:SKLabelNode!
    
    func clicked(point:CGPoint) {
        AudioPlayer.playSound("ButtonClick")
        if point.x < 0 {
            tab1.color = highlightColor
            tab2.color = unHighlightColor
            tab1Label.fontColor = highlightTextColor
            tab2Label.fontColor = unHighlightTextColor
            delegateClicked(0)
        } else {
            tab1.color = unHighlightColor
            tab2.color = highlightColor
            tab1Label.fontColor = unHighlightTextColor
            tab2Label.fontColor = highlightTextColor
            delegateClicked(1)
        }
    }
    
    convenience init(size:CGSize, clicked:@escaping ((_ tabIndex:Int)->Void)) {
        self.init(color: UIColor.clear, size: size)
        delegateClicked = clicked
        
        let cropNode = SKCropNode()
        let maskNode = SKShapeNode(rectOf: size, cornerRadius: 6)
        maskNode.strokeColor = UIColor.white
        maskNode.fillColor = UIColor.white
        maskNode.lineWidth = 1.0
        cropNode.maskNode = maskNode
        
        let tabContainer = SKSpriteNode(color: UIColor.clear, size: size)
        tabContainer.position = CGPoint(x: 0, y: 0)
        cropNode.addChild(tabContainer)
        cropNode.position = CGPoint(x:0, y:0)
        self.addChild(cropNode)
        
        //White and grey rectangles
        let tab1 = SKSpriteNode(color: highlightColor, size: CGSize(width:115 * Metrics.factor, height:22.5 * Metrics.factor))
        tab1.position = CGPoint(x:-115 * Metrics.factor * Metrics.half,y:0)
        tabContainer.addChild(tab1)
        let tab1Label = SKLabelNode(text:"Settings")
        tab1Label.verticalAlignmentMode = .center
        tab1Label.horizontalAlignmentMode = .center
        tab1Label.fontName = "ProximaNova-Regular"
        tab1Label.fontColor = highlightTextColor
        tab1Label.fontSize = 15 * Metrics.factor
        tab1Label.position = CGPoint(x: 0, y: 0)
        self.tab1Label = tab1Label
        tab1.addChild(tab1Label)
        self.tab1 = tab1
        
        let tab2 = SKSpriteNode(color: unHighlightColor, size: CGSize(width:115 * Metrics.factor, height:22.5 * Metrics.factor))
        tab2.position = CGPoint(x:115 * Metrics.factor * Metrics.half,y:0)
        tabContainer.addChild(tab2)
        let tab2Label = SKLabelNode(text:"Others")
        tab2Label.verticalAlignmentMode = .center
        tab2Label.horizontalAlignmentMode = .center
        tab2Label.fontName = "ProximaNova-Regular"
        tab2Label.fontColor = unHighlightTextColor
        tab2Label.fontSize = 15 * Metrics.factor
        tab2Label.position = CGPoint(x: 0, y: 0)
        self.tab2Label = tab2Label
        tab2.addChild(tab2Label)
        self.tab2 = tab2
    }
    
}
