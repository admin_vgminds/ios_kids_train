//
//  Migrator.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 26/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation

class Migrator {
    
    static fileprivate func move(_ oldURL: URL, _ newURL: URL) {
        do {
            let fileManager = FileManager.default
            try fileManager.copyItem(at: oldURL, to: newURL)
            try fileManager.removeItem(at: oldURL)
        } catch {
            print("Could not migrate old file")
        }
    }
    
    static fileprivate func exists(_ url: URL) -> Bool {
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: url.path) {
            print("Migrating ignored as already migrated")
            return true
        }
        return false
    }
    
    static func migrate(oldVideoPath:String, oldVideoFileMap:Dictionary<String,Any>) {
        let oldURL = URL(fileURLWithPath: oldVideoPath)
        let oldName = oldURL.lastPathComponent
        if let newName = oldVideoFileMap[oldName] {
            print("Migrating \(oldName) to \(newName)")
            let newURL = AssetManager.assetUrlFor(newName as! String)
            if !exists(newURL) {
                move(oldURL, newURL)
            }
        } else {
            print("Mapping not found to migrate old file \(oldName)")
        }
    }
    
    static func migrateOldFormatVideos(_ oldVideoFileMap:Dictionary<String,Any>) {
        let documentsDirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        let oldVideoPaths = getVideoFileURLs(path: "\(documentsDirPath!)/PlayLocalFolder/")
        for oldVideoPath in oldVideoPaths {
            migrate(oldVideoPath: oldVideoPath, oldVideoFileMap: oldVideoFileMap)
        }
    }
    
    static func getVideoFileURLs(path:String) -> [String] {
        do {
            var dir:ObjCBool = false
            FileManager.default.fileExists(atPath: path, isDirectory: &dir)
            if !dir.boolValue {
                return [path]
            }
            let pathContents = try FileManager.default.contentsOfDirectory(atPath: path)
            let pathUrl = URL(fileURLWithPath: path, isDirectory: true)
            var urls:[String] = []
            for subPath in pathContents {
                let subPathUrl = URL(fileURLWithPath: subPath, relativeTo: pathUrl)
                urls.append(contentsOf: getVideoFileURLs(path: subPathUrl.path))
            }
            return urls;
        } catch {
            return []
        }
    }
}
