//
//  AutoVideoDownloader.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 30/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation

class AutoVideoDownloader {
    
    static let sharedInstance:AutoVideoDownloader = AutoVideoDownloader()

    private init() {}
    
    func next(_ callback:@escaping ((_ percent:Int, _ videoId:Int)->Void)) {
        let videos = DataLoader.sharedInstance.videos!
        let videosCount = videos.count
        for i in 0..<videosCount {
            let videoInfo = videos[i]
            if (!(videoInfo["downloaded"] as! Bool)) {
                let videoUrl = videoInfo["VideoUrl"] as! String
                FileDownloadQueue.sharedInstance.download(url: videoUrl) { percent in
                    callback(percent, i)
                }
                return
            }
        }
    }
}
