//
//  JSONDataLoader.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 26/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation

class JSONDataLoader {
    
    static func loadData(_ key: String, _ dataFileName: String) -> Dictionary<String, Any> {
        var versionInfo:Dictionary<String, Any>
        let savedVersionInfo = UserDefaults.standard.dictionary(forKey: key)
        let bundledVersionInfo = readJSONFile(dataFileName, ext: "json")
        if savedVersionInfo == nil {
            UserDefaults.standard.set(bundledVersionInfo, forKey: key)
            versionInfo = bundledVersionInfo
        } else if !self.isRhsVersionNewerOrSame(lhs:bundledVersionInfo, rhs:savedVersionInfo!) {
            UserDefaults.standard.set(bundledVersionInfo, forKey: key)
            versionInfo = bundledVersionInfo
        } else {
            versionInfo = savedVersionInfo!
        }
        return versionInfo
    }
    
    static func readJSONFile(_ name:String, ext:String) -> Dictionary<String, Any> {
        var arr:Dictionary<String, Any>
        do {
            guard let url = Bundle.main.url(forResource: name, withExtension: ext) else {
                return [:]
            }
            let data = try Data(contentsOf: url)
            let rawArr = try JSONSerialization.jsonObject(with: data, options: [])
            arr = rawArr as! Dictionary<String, Any>
        } catch {
            arr = [:]
        }
        return arr
    }
    
    static func getDouble(value:Any?, defaultValue:Double) -> Double {
        if value == nil {
            return defaultValue
        } else if value is Double {
            return value as! Double
        } else if value is String {
            return Double(value as! String)!
        } else if value is Int {
            return Double(value as! Int)
        }
        return defaultValue
    }
    
    static func isRhsVersionNewerOrSame(lhs:Dictionary<String,Any>, rhs:Dictionary<String,Any>) -> Bool {
        return self.getDouble(value:lhs["Version"],defaultValue:0) <= self.getDouble(value:rhs["Version"],defaultValue:0)
    }
    
}
