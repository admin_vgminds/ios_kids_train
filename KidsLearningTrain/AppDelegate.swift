//
//  AppDelegate.swift
//  Kids App
//
//  Created by Vikram Rao on 02/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let locale = Locale.current
    var window: UIWindow?
    var backgroundSessionCompletionHandler: (() -> Void)?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UserDefaults.standard.register(defaults: ["settings-sounds" : true, "settings-music" : true, "settings-autoplay" : true])
        UIApplication.shared.isIdleTimerDisabled = false 
        registerAudioPlaybackType()
        FirebaseApp.configure()
        initOneSignal(launchOptions: launchOptions)
        return true
    }
    
    fileprivate func registerAudioPlaybackType() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
    }
    
    func initOneSignal(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "39376327-fe10-4f19-8cd6-7e85f3efe299",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        backgroundSessionCompletionHandler = completionHandler
    }
}

