//
//  FileDownloadQueue.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 30/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation

class FileDownloadQueue {
    
    static let sharedInstance:FileDownloadQueue = FileDownloadQueue()
    
    var queue:[String:FileDownloader] = [:]
    var callbacks:[String:((_ percentProgress:Int)->Void)] = [:]

    private init() {
        
    }
    
    func download(url:String, callback:@escaping ((_ percentProgress:Int)->Void)) {
        if isDownloading(url: url) {
            attach(url: url, callback: callback)
            return;
        }
        print("FileDownloadQueue: Downloading \(url)")
        let fileDownloader = FileDownloader()
        queue[url] = fileDownloader
        callbacks[url] = callback
        fileDownloader.download(url: url) { progress in
            self.callbacks[url]?(progress)
            if progress == -1 || progress == 100 {
                self.queue.removeValue(forKey: url)
            }
        }
    }
    
    func isDownloading(url:String) -> Bool {
        return queue.keys.contains(url)
    }
    
    func attach(url:String, callback:@escaping ((_ percentProgress:Int)->Void)) {
        print("FileDownloadQueue: Attaching to \(url)")
        if isDownloading(url: url) {
            callbacks[url] = callback
        }
    }
}
