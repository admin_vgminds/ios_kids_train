//
//  FileDownloader.swift
//  Kids App
//
//  Created by Vikram Rao on 05/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit

class FileDownloader : NSObject, URLSessionDownloadDelegate {

    private var callback:((_ percentProgress:Int)->Void)!
    private var session:URLSession!
    private var task:URLSessionDownloadTask!
    private var fileName:String!
    
    func download(url:String, callback:@escaping ((_ percentProgress:Int)->Void)) {
        self.callback = callback
        if !Reachability.isConnectedToNetwork() {
            print("ERROR: No internet connection")
            callbackExec(-1)
            return;
        }
        if task != nil {
            print("ERROR: A download is already scheduled in this object. Create new one.")
            callbackExec(-1)
            return
        }
        print("Downloading - \(url)")
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "FileDownloader\(url.hashValue)\(Date().timeIntervalSince1970)")
        session = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        if let url = URL(string: url) {
            fileName = url.lastPathComponent
            task = session.downloadTask(with: url)
            task.resume()
        } else {
            callbackExec(-1)
        }
    }

    fileprivate func callbackExec(_ percent:Int) {
        if percent == 100 {
            session.invalidateAndCancel()
        }
        callback(percent)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let destinationURLForFile = AssetManager.assetUrlFor(fileName)
        let fileManager = FileManager()
        if fileManager.fileExists(atPath: destinationURLForFile.path) {
            callbackExec(100)
            return
        }
        do {
            try fileManager.moveItem(at: location, to: destinationURLForFile)
            callbackExec(100)
        } catch let error {
            print("An error occurred while moving file to destination url - \(error.localizedDescription)")
            callbackExec(-1)
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let percentProgress = Int(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite) * 100)
        if percentProgress < 100 {
            callbackExec(percentProgress)
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if error != nil {
            callbackExec(-1)
        }
    }
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
                let completionHandler = appDelegate.backgroundSessionCompletionHandler {
                appDelegate.backgroundSessionCompletionHandler = nil
                completionHandler()
            }
        }
    }
}
