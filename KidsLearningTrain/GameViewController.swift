//
//  GameViewController.swift
//  Kids App
//
//  Created by Vikram Rao on 02/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController, ThumbnailCardDelegate {
  
    override func viewDidLoad() {
        super.viewDidLoad()
        if let view = self.view as! SKView? {
            let scene = GameScene(size: view.bounds.size)
            scene.scaleMode = .resizeFill
            scene.thumbNailDelegate = self
            view.presentScene(scene)
            view.showsFPS = false
            view.showsNodeCount = false
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func thumbNailCardDidTap(videoIndex:Int) {
        let videoVC = storyboard?.instantiateViewController(withIdentifier: "video-player") as! VideoPlayerController
        videoVC.videos = DataLoader.sharedInstance.videos
        videoVC.videoIndex = videoIndex
        AudioPlayer.stop()
        AudioPlayer.playSound("ButtonClick")
        navigationController?.pushViewController(videoVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AudioPlayer.playMusic("BGMusic")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "VIEW_WILL_APPEAR"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        print("Memory Warning")
        super.didReceiveMemoryWarning()
    }
}
