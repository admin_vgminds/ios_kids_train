//
//  SplashViewController.swift
//  Kids App
//
//  Created by Vikram Rao on 04/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Metrics.factor = view.bounds.height / Metrics.baseHeight
        Metrics.navBarHeight = max(Metrics.navBarHeight, Metrics.navBarHeight * Metrics.factor * Metrics.half)
        Thumbnail.VerticalMargin *= (view.bounds.height >= 768 ? Metrics.factor * 1.5 : Metrics.factor)
        Thumbnail.ContentEdgePadding *= Metrics.factor
        Thumbnail.HorizontalPadding *= Metrics.factor
        Thumbnail.ShadowSize *= Metrics.factor
        Thumbnail.ImagePadding *= Metrics.factor
        Thumbnail.InfoSectionHeight *= Metrics.factor
        Thumbnail.ButtonWidth *= Metrics.factor
        Thumbnail.InfoSectionBottomPadding *= Metrics.factor
        Thumbnail.PlayIconWidth *= Metrics.factor
        Thumbnail.FreeLabelWidth *= Metrics.factor
        Popup.closeButtonWidth *= Metrics.factor
        Popup.closeButtonPadding *= Metrics.factor
        Settings.pointerWidth *= Metrics.factor
        Settings.pointerHeight *= Metrics.factor
        Settings.eraseButtonWidth *= Metrics.factor
        Settings.eraseButtonHeight *= Metrics.factor
        Settings.eraseButtonBottomSpacing *= Metrics.factor
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DataLoader.sharedInstance.refresh {
            DataLoader.sharedInstance.cacheIcons {
                let gameVC = self.storyboard?.instantiateViewController(withIdentifier: "game-home")
                self.navigationController?.pushViewController(gameVC!, animated: true)
            }
        }
    }

}
