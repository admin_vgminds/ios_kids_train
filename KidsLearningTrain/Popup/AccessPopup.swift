//
//  AccessPopup.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 23/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class AccessPopup: PopUp {
    
    var onSuccess:(()->Void)!
    var captchaText = ""
    var currentText:String = ""
    let numberToText = [
        "1" : "One",
        "2" : "Two",
        "3" : "Three",
        "4" : "Four",
        "5" : "Five",
        "6" : "Six",
        "7" : "Seven",
        "8" : "Eight",
        "9" : "Nine"
    ]
    var selectedButtons:[Button] = []
    let buttonUnSelectColor = UIColor(rgbColorCodeRed: 77, green: 130, blue: 149, alpha: 1.0)
    let buttonSelectColor = UIColor(rgbColorCodeRed: 73, green: 192, blue: 49, alpha: 1.0)
    let buttonClickedColor = UIColor(rgbColorCodeRed: 0, green: 67, blue: 91, alpha: 1.0)
    var captcha:SKShapeNode!
    
    override func present(scene: SKScene) {
        super.present(scene: scene)
        AudioPlayer.playSound("CallTheGrownUpsToHelp")
    }
    
    fileprivate func onCorrectNumber(button:Button) {
        button.setBackgroundColor(self.buttonSelectColor)
        self.selectedButtons.append(button)
        if (self.captchaText == self.currentText) {
            self.remove(animate: true, onComplete: {
                if let onSuccess = self.onSuccess {
                    onSuccess()
                }
            })
        }
    }
    
    fileprivate func isSelected(_ button: Button) -> Bool {
        for selectedbutton in selectedButtons {
            if selectedbutton == button {
                return true
            }
        }
        return false
    }
    
    fileprivate func onNumberEntered(button:Button, name: String?) {
        let newText = self.numberToText[name!] ?? ""
        let probableText = self.currentText.count > 0 ? "\(self.currentText) \(newText)" : newText
        if self.captchaText.hasPrefix(probableText) {
            self.currentText = probableText
            onCorrectNumber(button:button)
            return
        }
        if !isSelected(button) {
            button.setBackgroundColor(self.buttonUnSelectColor)
        }
    }
    
    fileprivate func addNumPadButton(_ i: Int, _ baseY: CGFloat) {
        let lineNum = ((i-1) / 3)
        let padding:CGFloat = CGFloat(12.5) * Metrics.factor
        let buttonHeight:CGFloat = CGFloat(31.5) * Metrics.factor
        let buttonWidth:CGFloat = CGFloat(30) * Metrics.factor
        
        let button = Button(backgroundColor: UIColor.white, color: buttonUnSelectColor, size: CGSize(width: buttonWidth, height: buttonHeight), name: "\(i)", fontName: "ProximaNova-Semibold", fontColor: UIColor.white, fontSize: 19.8 * Metrics.factor, radius:5 * Metrics.factor)
        let x:CGFloat = CGFloat(((i-1) % 3) - 1) * (padding + buttonWidth)
        let y:CGFloat = baseY - CGFloat(lineNum) * (padding + buttonHeight) - padding - (buttonHeight * Metrics.half)
        button.position = CGPoint(x: x ,y: y)
        button.onBeforeClickAnimation = {
            if !self.isSelected(button) {
                button.setBackgroundColor(self.buttonClickedColor)
            }
        }
        button.onClick {name in
            self.onNumberEntered(button:button, name:name)
        }
        button.animateClick = true
        contentArea.addChild(button)
    }
    
    fileprivate func generateCaptcha() {
        captchaText = ""
        for _ in 1...3 {
            var rs = ""
            repeat {
                let r = (arc4random_uniform(9)+1)
                rs = self.numberToText["\(r)"] ?? ""
            } while captchaText.contains(rs)
            captchaText = captchaText.count > 0 ? "\(captchaText) \(rs)" : "\(rs)"
        }
    }
    
    fileprivate func showRandomCaptcha() {
        captcha.removeAllChildren()
        generateCaptcha()
        let captcaLabel = SKLabelNode(text: self.captchaText)
        captcaLabel.position = CGPoint.zero
        captcaLabel.fontName = "ProximaNova-Regular"
        captcaLabel.fontSize = 15.2 * Metrics.factor
        captcaLabel.fontColor = UIColor(rgbColorCodeRed: 0, green: 107, blue: 146, alpha: 1)
        captcaLabel.horizontalAlignmentMode = .center
        captcaLabel.verticalAlignmentMode = .center
        captcha.addChild(captcaLabel)
    }
    
    init(view: SKView) {
        let size = CGSize(width:234 * Metrics.factor, height:265 * Metrics.factor)
        super.init(view: view, bgColor: UIColor.white, size: size, title: "Parental Lock", headerImageName: "parental-control-header")
        
        let captchaBgColor = UIColor(rgbColorCodeRed: 212, green: 244, blue: 255, alpha: 1.0)
        let captchaWidth:CGFloat = 163 * Metrics.factor
        let captchaHeight:CGFloat = 26.5 * Metrics.factor
        
        captcha = SKShapeNode(rectOf: CGSize(width: captchaWidth, height: captchaHeight), cornerRadius: 5 * Metrics.factor)
        captcha.strokeColor = captchaBgColor
        captcha.fillColor = captchaBgColor
        captcha.position = CGPoint(x: 0, y: size.height * Metrics.half - 99.5 * Metrics.factor)
        contentArea.addChild(captcha)
        
        showRandomCaptcha()
        
        let baseY:CGFloat = captcha.position.y - captchaHeight * Metrics.half
        for i in 1...9 {
            addNumPadButton(i, baseY)
        }
    }
    
    override func remove(animate:Bool = true, onComplete:(()->Void)? = nil) {
        super.remove(animate: animate) {
            if onComplete != nil {
                onComplete!()
            }
        }
    }
    
}
