//
//  EraseVideoConfirmPopup.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 26/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import SpriteKit

class EraseVideoConfirmPopup : PopUp {
    
    init(view:SKView) {        
        let width:CGFloat = CGFloat(193) * Metrics.factor
        let height:CGFloat = CGFloat(138.5) * Metrics.factor
        let halfWidth:CGFloat = width * Metrics.half
        let halfHeight:CGFloat = height * Metrics.half
        
        let size = CGSize(width:width, height:height)
        super.init(view: view, bgColor: UIColor.white, size: size)
        
        let fontSize:CGFloat = 15 * Metrics.factor
        
        let messageLine1 = SKLabelNode(text: "Are you sure you want")
        messageLine1.verticalAlignmentMode = .center
        messageLine1.horizontalAlignmentMode = .center
        messageLine1.fontName = "ProximaNova-Regular"
        messageLine1.fontColor = UIColor.black
        messageLine1.fontSize = fontSize
        messageLine1.position = CGPoint(x: 0, y: halfHeight - 44 * Metrics.factor - fontSize * Metrics.half)
        contentArea.addChild(messageLine1)
        
        let messageLine2 = SKLabelNode(text: "to erase all video’s?")
        messageLine2.verticalAlignmentMode = .center
        messageLine2.horizontalAlignmentMode = .center
        messageLine2.fontName = "ProximaNova-Regular"
        messageLine2.fontColor = UIColor.black
        messageLine2.fontSize = fontSize
        messageLine2.position = CGPoint(x: 0, y: halfHeight - 44 * Metrics.factor - fontSize * Metrics.half - CGFloat(18) * Metrics.factor)
        contentArea.addChild(messageLine2)
        
        let buttonWidth:CGFloat = CGFloat(78) * Metrics.factor
        let buttonHeight:CGFloat = CGFloat(24) * Metrics.factor
        let buttonFont:CGFloat = CGFloat(16) * Metrics.factor
        let halfButtonWidth:CGFloat = buttonWidth * Metrics.half
        let halfButtonHeight:CGFloat = buttonHeight * Metrics.half
        let buttonBottomPadding:CGFloat = CGFloat(22) * Metrics.factor
        let buttonSidePadding:CGFloat = CGFloat(13.5) * Metrics.factor
        
        let deleteButton = Button(imageNamed:"green-half-button-bg", size:CGSize(width:buttonWidth, height:buttonHeight), name:"Yes", fontName:"ProximaNova-Regular", fontColor:UIColor.white, fontSize:buttonFont)
        deleteButton.position = CGPoint(x:-halfWidth + buttonSidePadding + halfButtonWidth, y:-halfHeight + buttonBottomPadding + halfButtonHeight)
        deleteButton.onClick {name in
            self.remove()
            AssetManager.clearAssetVideos()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ASSETS_CLEARED"), object: nil)
        }
        contentArea.addChild(deleteButton)
        
        let cancelButton = Button(imageNamed:"red-half-button-bg", size:CGSize(width:buttonWidth, height:buttonHeight), name:"No", fontName:"ProximaNova-Regular", fontColor:UIColor.white, fontSize:buttonFont)
        cancelButton.position = CGPoint(x:halfWidth - buttonSidePadding - halfButtonWidth, y:-halfHeight + buttonBottomPadding + halfButtonHeight)
        cancelButton.onClick {name in
            self.remove()
        }
        contentArea.addChild(cancelButton)
    }
}
